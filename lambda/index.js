const Alexa = require('ask-sdk');
const TABLE_NAME = 'myScore';

let skill;
exports.handler = async function (event, context) {
  if (skill) return skill.invoke(event);
  skill = Alexa.SkillBuilders.standard()
    .addRequestHandlers(
      LaunchRequestHandler,
      NumberIntentHandler,
      CancelIntentHandler)
    .withTableName(TABLE_NAME)
    .withAutoCreateTable(true)
    .create();
  return skill.invoke(event);
};

const TITLE = 'かけ算ゲーム';

function getResponse(h, msg) {
  return h.responseBuilder
    .speak(msg).reprompt(msg).withSimpleCard(TITLE, msg)
    .getResponse();
}

async function getHighScore(h) {
  const pa = await h.attributesManager
    .getPersistentAttributes();
  // スコア初期化
  if (!pa.highScore) {
    pa.highScore = 0;
    setHighScore(h, pa.highScore);
  }
  console.log('high score=' + pa.highScore);
  return pa.highScore;
}

async function setHighScore(h, value) {
  const pa = await h.attributesManager
    .getPersistentAttributes();
  pa.highScore = value;
  h.attributesManager.setPersistentAttributes(pa);
  await h
    .attributesManager
    .savePersistentAttributes();
}

const LaunchRequestHandler = {
  canHandle(h) {
    const type = h.requestEnvelope.request.type;
    return (type === 'LaunchRequest');
  },
  async handle(h) {
    const attr = h.attributesManager.getSessionAttributes();
    attr.score = 0;   // 初期点数設定
    const highScore = await getHighScore(h);
    attr.one = Math.floor(Math.random() * 10) + 1;
    attr.two = Math.floor(Math.random() * 10) + 1;
    let question_str = String(attr.one) + 'かけ' + String(attr.two) + 'は？';
    return getResponse(h, 
      TITLE + 'にようこそ。' + 
      'これまでのスコアは、' + highScore + '点です。' +
      question_str);
  }
};

const NumberIntentHandler = {
  canHandle(h) {
    const type = h.requestEnvelope.request.type;
    const name = h.requestEnvelope.request.intent.name;
    return (type === 'IntentRequest') && 
           (name === 'NumberIntent');
  },
  async handle(h) {
    const attr = h.attributesManager.getSessionAttributes();
    const highScore = await getHighScore(h);
    const answer = attr.one * attr.two;
    const req = h.requestEnvelope.request;
    const user = parseInt(req.intent.slots.num.value);
    let msg = '';
    if (answer === user) {
      attr.score += 1;
      msg += '正解！';
    } else {
      msg += 'はずれです。';
      msg += '正解は、' + answer + 'でした。';
      attr.score -= 1;
    }
    msg += '現在、' + attr.score + '点です。';

    attr.one = Math.floor(Math.random() * 10) + 1;
    attr.two = Math.floor(Math.random() * 10) + 1;
    let question_str = String(attr.one) + 'かけ' + String(attr.two) + 'は？';
    msg += question_str;
    return getResponse(h, msg);
  }
};

const CancelIntentHandler = {
  canHandle(h) {
    const type = h.requestEnvelope.request.type;
    const name = h.requestEnvelope.request.intent.name;
    return (type === 'IntentRequest') && 
           (name === 'AMAZON.CancelIntent' ||
            name === 'AMAZON.StopIntent')
  },
　async handle(h) {
    const attr = h.attributesManager.getSessionAttributes();
    const highScore = await getHighScore(h);
    const result_score = attr.score + highScore;
    await setHighScore(h, result_score);
    const msg = '終わります。' + 
    '今回' + attr.score +  '点取得しました。' + 
    '現在までの累積スコアは、' + result_score + '点です。';
    return h.responseBuilder.speak(msg).getResponse();
  }
}