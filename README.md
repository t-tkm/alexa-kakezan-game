# alexa-kakezan-game
## Scenario
![AWS-Alexa-sample-scenario.png](/imgs/AWS-Alexa-sample-scenario.png)

## Architecture
![AWS-Alexa-sample-architecture.png](/imgs/AWS-Alexa-sample-architecture.png)

## Impl
![AWS-Alexa-sample-impl.png](/imgs/AWS-Alexa-sample-impl.png)
